<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class MC extends CI_Controller
{

function __construct()
{
     parent::__construct();
     $this->load->model('MainModel','mm');
}

public function index()
{
	$this->load->view('homepage');
}
//load admin page 
public function adminsView()
{
	$admin_list['admins']=$this->mm->activeAdminsList();
	$this->load->view('admin/admin_registration',$admin_list);
}

//admin profile
public function adminMore()
{
	$staffId=$this->input->post('staffId', TRUE);
	$admin_profile['admin_profile']=$this->mm->adminProfile($staffId);
	$this->load->view('admin/admin_profile',$admin_profile);
}
//admin Profile Update
public function editAdmin()
{
	$staffId=$this->input->post('staffId', TRUE);
	$admin_profile['admin_profile']=$this->mm->adminProfile($staffId);
	$this->load->view('admin/edit_admin',$admin_profile);
}
//admin registration 
public function newAdmin()
{
	//admin registration
	$staff_id=$this->input->post('staffID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$admin_nid=$this->input->post('nationalID', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$admin_username=$this->input->post('userName', TRUE);
	$password=md5($admin_username.$staff_id);//default password
	$dateRegistered= date("Y-m-d"); 
	//create an array of the data to be inserted at once
	$admin_details = array('admin_staff_id' => $staff_id, 'admin_fname'=>$first_name, 'admin_lname'=>$last_name, 'admin_other_names'=>$other_names, 'admin_nid'=>$admin_nid,'admin_phone'=>$phone_no,'admin_email'=>$email, 'admin_username'=>$admin_username,'date_registered'=>$dateRegistered,'password'=>$password);
	
	$this->db->select('*');
	$this->db->from('admin');
	$this->db->where('admin_nid',$admin_nid);
	$this->db->or_where('admin_staff_id',$staff_id);
	$this->db->or_where('admin_username',$admin_username);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate National/Staff ID/Username",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/adminsView')));
        }else 
            {
            	$this->db->select('*');
				$this->db->from('admin');
				$this->db->where('admin_username',$admin_username);
				$query = $this->db->get();
	            $num=$query->num_rows(); 
	            if($num>0)
		            {
		            	$feedback = array('error' => "Duplicate username",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/adminsView')));
		            }else
		            	{
					            	$result=$this->mm->newAdmin($admin_details);
									if($result)
										{
											$feedback = array('error' => "",'success' => "New admin added");
											$this->session->set_flashdata('msg',$feedback);
						                   redirect(base_url(('MC/adminsView')));
										}else 
											{
												$feedback = array('error' => "Registration failed",'success' => "");
												$this->session->set_flashdata('msg',$feedback);
							                   redirect(base_url(('MC/adminsView')));
											}
            			}
           }

}
//update admin
public function updateAdmin()
{
	$admin_nid=$this->input->post('nationalID', TRUE);
	$staff_id=$this->input->post('staffID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$admin_username=$this->input->post('userName', TRUE);
	//create an array of the data to be inserted at once
	$admin_details = array('admin_staff_id' => $staff_id, 'admin_fname'=>$first_name, 'admin_lname'=>$last_name, 'admin_other_names'=>$other_names,'admin_phone'=>$phone_no,'admin_email'=>$email, 'admin_username'=>$admin_username);
	$this->db->select('admin_staff_id');
    $this->db->from('admin');
    $this->db->where('admin_staff_id',$staff_id);
    $result=$this->db->get_compiled_select();

    $this->db->select('admin_staff_id');
    $this->db->from('admin');
    $this->db->where("`admin_staff_id` NOT IN ($result)", NULL, FALSE);
	$this->db->where('admin_username ',$admin_username);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate Username",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/adminsView')));
        }else{
        		$this->db->select('admin_staff_id');
			    $this->db->from('admin');
			    $this->db->where("`admin_staff_id` NOT IN ($result)", NULL, FALSE);
				$this->db->where('admin_username ',$admin_nid);
				$query = $this->db->get();
			    $num=$query->num_rows(); 
			    if($num>0)
			        {
			        	$feedback = array('error' => "Duplicate National ID",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/adminsView')));
			        }else{
				        	$result=$this->mm->updateAdmin($admin_details,$staff_id);
							if($result)
								{
									$feedback = array('error' => "",'success' => "Updated");
									$this->session->set_flashdata('msg',$feedback);
						           redirect(base_url(('MC/adminsView')));
								}else 
									{
										$feedback = array('error' => "No Changes",'success' => "");
										$this->session->set_flashdata('msg',$feedback);
						               redirect(base_url(('MC/adminsView')));
									}
						}	

			}

}
//disable admin
public function disableAdmin()
{
	$staffId=$this->input->post('staffId', TRUE);
	$reasonToDisable=$this->input->post('reasonToDisable', TRUE);
	$updateDetails=array('active_status'=>0, 'reason_inactive'=>$reasonToDisable);
	$result=$this->mm->disableAdmin($updateDetails,$staffId);
	if($result)
		{
			$feedback = array('error' => "",'success' => "admin disabled");
			$this->session->set_flashdata('msg',$feedback);
           redirect(base_url(('MC/adminsView')));
		}else 
			{
				$feedback = array('error' => "Failed to disable",'success' => "");
				$this->session->set_flashdata('msg',$feedback);
               redirect(base_url(('MC/adminsView')));
			}
}
//load officers page 
public function officersView()
{
	$officers_list['officers']=$this->mm->activeOfficersList();
	$this->load->view('admin/officer_registration',$officers_list);
}
//officer profile
public function officerProfile()
{
	$staffId=$this->input->post('staffId', TRUE);
	$officer_profile['officer_profile']=$this->mm->officerProfile($staffId);
	$this->load->view('admin/officer_profile',$officer_profile);
}
//Officer Profile Update
public function editOfficer()
{
	$staffId=$this->input->post('staffId', TRUE);
	$officer_profile['officer_profile']=$this->mm->officerProfile($staffId);
	$this->load->view('admin/edit_officer',$officer_profile);
}
//Officer registration 
public function newOfficer()
{
	//Officer registration
	$phaseId=$this->input->post('phaseId', TRUE);
	$staff_id=$this->input->post('staffID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$officer_nid=$this->input->post('nationalID', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$officer_username=$this->input->post('userName', TRUE);
	$password=md5($officer_username.$staff_id);
	$dateRegistered= date("Y-m-d"); 
	//create an array of the data to be inserted at once
	$officer_details = array('officer_phase_id'=>$phaseId,'officer_staff_id' => $staff_id, 'officer_fname'=>$first_name, 'officer_lname'=>$last_name, 'officer_other_names'=>$other_names, 'officer_nid'=>$officer_nid,'officer_phone'=>$phone_no,'officer_email'=>$email, 'officer_username'=>$officer_username,'date_registered'=>$dateRegistered,'password'=>$password);
	
	$this->db->select('*');
	$this->db->from('officers');
	$this->db->where('officer_nid',$officer_nid);
	$this->db->or_where('officer_staff_id',$staff_id);
	$this->db->or_where('officer_username',$officer_username);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate National/Staff ID",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/officersView')));
        }else 
            {
            	$this->db->select('*');
				$this->db->from('officers');
				$this->db->where('officer_username',$officer_username);
				$query = $this->db->get();
	            $num=$query->num_rows(); 
	            if($num>0)
		            {
		            	$feedback = array('error' => "Duplicate username",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/officersView')));
		            }else
		            	{
					            	$result=$this->mm->newOfficer($officer_details);
									if($result)
										{
											$feedback = array('error' => "",'success' => "New officer added");
											$this->session->set_flashdata('msg',$feedback);
						                   redirect(base_url(('MC/officersView')));
										}else 
											{
												$feedback = array('error' => "Registration failed",'success' => "");
												$this->session->set_flashdata('msg',$feedback);
							                   redirect(base_url(('MC/officersView')));
											}
            			}
           }

}
//update officer
public function updateOfficer()
{
	$phaseId=$this->input->post('phaseId', TRUE);
	$staffId=$this->input->post('staffID', TRUE);
	$officer_nid=$this->input->post('nationalID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$officer_username=$this->input->post('userName', TRUE);
	//create an array of the data to be inserted at once
	$officer_details = array('officer_phase_id'=>$phaseId,'officer_nid' => $officer_nid, 'officer_fname'=>$first_name, 'officer_lname'=>$last_name, 'officer_other_names'=>$other_names,'officer_phone'=>$phone_no,'officer_email'=>$email, 'officer_username'=>$officer_username);

	$this->db->select('officer_staff_id');
    $this->db->from('officers');
    $this->db->where('officer_staff_id',$staffId);
    $result=$this->db->get_compiled_select();

    $this->db->select('officer_staff_id');
    $this->db->from('officers');
    $this->db->where("`officer_staff_id` NOT IN ($result)", NULL, FALSE);
	$this->db->where('officer_username',$officer_username);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate Username",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/officersView')));
        }else
        	{
        		$this->db->select('officer_staff_id');
			    $this->db->from('officers');
			    $this->db->where("`officer_staff_id` NOT IN ($result)", NULL, FALSE);
				$this->db->where('officer_nid',$officer_nid);
				$query = $this->db->get();
			    $num=$query->num_rows(); 
			     if($num>0)
			        {
			        	$feedback = array('error' => "Duplicate National ID",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/officersView')));
			        }else
        					{
								$result=$this->mm->updateOfficer($officer_details,$staffId);
								if($result)
									{
										$feedback = array('error' => "",'success' => "Updated");
										$this->session->set_flashdata('msg',$feedback);
							           redirect(base_url(('MC/officersView')));
									}else 
										{
											$feedback = array('error' => "No Changes",'success' => "");
											$this->session->set_flashdata('msg',$feedback);
							               redirect(base_url(('MC/officersView')));
										}
							}
				}

}

//Disable  officer
public function disableOfficer()
{
	$staffId=$this->input->post('staffId', TRUE);
	$reasonToDisable=$this->input->post('reasonToDisable', TRUE);
	$updateDetails=array('active_status'=>0, 'reason_inactive'=>$reasonToDisable);
	$result=$this->mm->disableOfficer($updateDetails,$staffId);
	if($result)
		{
			$feedback = array('error' => "",'success' => "Officer disabled");
			$this->session->set_flashdata('msg',$feedback);
           redirect(base_url(('MC/officersView')));
		}else 
			{
				$feedback = array('error' => "Failed to disable",'success' => "");
				$this->session->set_flashdata('msg',$feedback);
               redirect(base_url(('MC/officersView')));
			}
}


//helpdesks view page
public function helpdesksView()
{
	$list['floors']=$this->mm->floorsList();
	$list['buildings']=$this->mm->buildingsList();
	$list['helpdesks']=$this->mm->helpdesksList();
	$this->load->view('officer/hd_registration',$list);
}

//Officers Dashboard
public function dashboard()
{
	$list['guests']=$this->mm->newGuestsList();
	$this->load->view('officer/guest_registration',$list);
}
public function newHelpdesk()
{
	//Helpdesk registration
	$phaseId=$this->input->post('phaseId', TRUE);
	$helpdeskName=$this->input->post('helpdeskName', TRUE);
	$buildingId=$this->input->post('buildingId', TRUE);
	$floorNo=$this->input->post('floorNo', TRUE);
	$dateRegistered= date("Y-m-d"); 
	//create an array of the data to be inserted at once
	$helpdesk_details = array('hd_phase_id'=>$phaseId,'hd_name' => $helpdeskName,'hd_building_id'=>$buildingId, 'hd_floor_no'=>$floorNo,'date_registered'=>$dateRegistered);
	
	$this->db->select('*');
	$this->db->from('helpdesks');
	$this->db->where('hd_phase_id',$phaseId);
	$this->db->where('hd_name',$helpdeskName);
	$this->db->where('hd_floor_no',$floorNo);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate Helpdesk",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/helpdesksView')));
        }else 
            {
            	
	        	$result=$this->mm->newHelpdesk($helpdesk_details);
				if($result)
					{
						$feedback = array('error' => "",'success' => "New helpdesk added");
						$this->session->set_flashdata('msg',$feedback);
	                   redirect(base_url(('MC/helpdesksView')));
					}else 
						{
							$feedback = array('error' => "Registration failed",'success' => "");
							$this->session->set_flashdata('msg',$feedback);
		                   redirect(base_url(('MC/helpdesksView')));
						}

           }

}
//helpdesk edit page
public function editHelpdesk()
{
	$hd_auto_id=$this->input->post('helpdeskId', TRUE);
	$list['floors']=$this->mm->floorsList();
	$list['buildings']=$this->mm->buildingsList();
	$list['helpdesks']=$this->mm->helpdesksList();
	$list['helpdesk']=$this->mm->getHelpdesk($hd_auto_id);
	$this->load->view('officer/edit_helpdesk',$list);
}

public function updateHelpdesk()
{
	//Helpdesk update
	$helpdeskId=$this->input->post('helpdeskId', TRUE);
	$phaseId=$this->input->post('phaseId', TRUE);
	$helpdeskName=$this->input->post('helpdeskName', TRUE);
	$buildingId=$this->input->post('buildingId', TRUE);
	$floorNo=$this->input->post('floorNo', TRUE);
	//create an array of the data to be inserted at once
	$helpdesk_details = array('hd_phase_id'=>$phaseId,'hd_name' => $helpdeskName,'hd_building_id'=>$buildingId, 'hd_floor_no'=>$floorNo);
            	
	$result=$this->mm->updateHelpdesk($helpdesk_details,$helpdeskId);
	if($result)
		{
			$feedback = array('error' => "",'success' => "Update successful");
			$this->session->set_flashdata('msg',$feedback);
           redirect(base_url(('MC/helpdesksView')));
		}else 
			{
				$feedback = array('error' => "No changes",'success' => "");
				$this->session->set_flashdata('msg',$feedback);
               redirect(base_url(('MC/helpdesksView')));
			}    

}

//load helpdesk officers page 
public function hdOfficersView()
{
	$list['hdofficers']=$this->mm->activeHdOfficersList();
	$list['helpdesks']=$this->mm->helpdesksList();
	$this->load->view('officer/hd_officer_registration',$list);
}

//Officer registration 
public function newHdOfficer()
{
	$helpdeskId=$this->input->post('helpdeskId', TRUE);
	$staff_id=$this->input->post('staffID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$hd_officer_nid=$this->input->post('nationalID', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$hd_officer_username=$this->input->post('userName', TRUE);
	$password=md5($hd_officer_username.$staff_id);
	$dateRegistered= date("Y-m-d"); 
	
	//create an array of the data to be inserted at once
	$hd_officer_details = array('hd_officer_hd_auto_id'=>$helpdeskId,'hd_officer_staff_id' => $staff_id, 'hd_officer_fname'=>$first_name, 'hd_officer_lname'=>$last_name, 'hd_officer_other_names'=>$other_names, 'hd_officer_nid'=>$hd_officer_nid,'hd_officer_phone'=>$phone_no,'hd_officer_email'=>$email, 'hd_officer_username'=>$hd_officer_username,'date_registered'=>$dateRegistered,'password'=>$password);
	
	$this->db->select('*');
	$this->db->from('hd_officers');
	$this->db->where('hd_officer_nid',$hd_officer_nid);
	$this->db->or_where('hd_officer_staff_id',$staff_id);
	$this->db->or_where('hd_officer_username',$hd_officer_username);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate National/Staff ID",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/hdOfficersView')));
        }else 
            {
            	$this->db->select('*');
				$this->db->from('hd_officers');
				$this->db->where('hd_officer_username',$hd_officer_username);
				$query = $this->db->get();
	            $num=$query->num_rows(); 
	            if($num>0)
		            {
		            	$feedback = array('error' => "Duplicate username",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/hdOfficersView')));
		            }else
		            	{
					            	$result=$this->mm->newHdOfficer($hd_officer_details);
									if($result)
										{
											$feedback = array('error' => "",'success' => "New officer added");
											$this->session->set_flashdata('msg',$feedback);
						                   redirect(base_url(('MC/hdOfficersView')));
										}else 
											{
												$feedback = array('error' => "Registration failed",'success' => "");
												$this->session->set_flashdata('msg',$feedback);
							                   redirect(base_url(('MC/hdOfficersView')));
											}
            			}
           }

}

//update Helpdesk officer
public function updateHdOfficer()
{
	$helpdeskId=$this->input->post('helpdeskId', TRUE);
	$staffId=$this->input->post('staffID', TRUE);
	$hd_officer_nid=$this->input->post('nationalID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$hd_officer_username=$this->input->post('userName', TRUE);
	//create an array of the data to be inserted at once
	$hd_officer_details = array('hd_officer_hd_auto_id'=>$helpdeskId,'hd_officer_nid' => $hd_officer_nid, 'hd_officer_fname'=>$first_name, 'hd_officer_lname'=>$last_name, 'hd_officer_other_names'=>$other_names,'hd_officer_phone'=>$phone_no,'hd_officer_email'=>$email, 'hd_officer_username'=>$hd_officer_username);

	$this->db->select('hd_officer_staff_id');
    $this->db->from('hd_officers');
    $this->db->where('hd_officer_staff_id',$staffId);
    $result=$this->db->get_compiled_select();

    $this->db->select('hd_officer_staff_id');
    $this->db->from('hd_officers');
    $this->db->where("`hd_officer_staff_id` NOT IN ($result)", NULL, FALSE);
	$this->db->where('hd_officer_username',$hd_officer_username);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate Username",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/hdOfficersView')));
        }else
        	{
        		$this->db->select('hd_officer_staff_id');
			    $this->db->from('hd_officers');
			    $this->db->where("`hd_officer_staff_id` NOT IN ($result)", NULL, FALSE);
				$this->db->where('hd_officer_nid',$hd_officer_nid);
				$query = $this->db->get();
			    $num=$query->num_rows(); 
			     if($num>0)
			        {
			        	$feedback = array('error' => "Duplicate National ID",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/hdOfficersView')));
			        }else
        					{
								$result=$this->mm->updateHdOfficer($hd_officer_details,$staffId);
								if($result)
									{
										$feedback = array('error' => "",'success' => "Updated");
										$this->session->set_flashdata('msg',$feedback);
							           redirect(base_url(('MC/hdOfficersView')));
									}else 
										{
											$feedback = array('error' => "No Changes",'success' => "");
											$this->session->set_flashdata('msg',$feedback);
							               redirect(base_url(('MC/hdOfficersView')));
										}
							}
				}

}
//Officer Helpdesk Profile Update
public function editHdOfficer()
{
	$staffId=$this->input->post('staffId', TRUE);
	$list['helpdesks']=$this->mm->helpdesksList();
	$list['hd_officer_profile']=$this->mm->hdOfficerProfile($staffId);
	$this->load->view('officer/edit_hd_officer',$list);
}
//Disable Helpdesk officer
public function disableHdOfficer()
{
	$staffId=$this->input->post('staffId', TRUE);
	$reasonToDisable=$this->input->post('reasonToDisable', TRUE);
	$updateDetails=array('hd_active_status'=>0, 'hd_reason_inactive'=>$reasonToDisable);
	$result=$this->mm->disableHdOfficer($updateDetails,$staffId);
	if($result)
		{
			$feedback = array('error' => "",'success' => "Officer disabled");
			$this->session->set_flashdata('msg',$feedback);
           redirect(base_url(('MC/hdOfficersView')));
		}else 
			{
				$feedback = array('error' => "Failed to disable",'success' => "");
				$this->session->set_flashdata('msg',$feedback);
               redirect(base_url(('MC/hdOfficersView')));
			}
}
public function guestsView()
{
	$list['guests']=$this->mm->guestsList();
	$this->load->view('officer/all_guests',$list);
}
public function newGuests()
{
	$list['guests']=$this->mm->newGuestsList();
	$this->load->view('officer/guest_registration',$list);
}

//Officer registration 
public function newGuest()
{
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$idNo=$this->input->post('IdNo', TRUE);
	$idType=$this->input->post('idType', TRUE);
	$phoneNumber=$this->input->post('phoneNumber', TRUE);

	$dateRegistered= date("Y-m-d"); 
	
	//create an array of the data to be inserted at once
	$guest_details = array('guest_fname'=>$first_name, 'guest_lname'=>$last_name, 'guest_other_names'=>$other_names, 'guest_id'=>$idNo,'guest_id_type'=>$idType,'guest_phone'=>$phoneNumber,'date_registered'=>$dateRegistered);
	
	$this->db->select('*');
	$this->db->from('guests');
	$this->db->where('guest_id',$idNo);
	$this->db->or_where('guest_phone',$phoneNumber);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate ID/Phone No",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/newGuests')));
        }else 
            {
            	$result=$this->mm->newGuest($guest_details);
				if($result)
					{
						$feedback = array('error' => "",'success' => "New guest added",'state'=>array('fullName'=>"",'recordId'=>""));
						$this->session->set_flashdata('msg',$feedback);
	                   redirect(base_url(('MC/newGuests')));
					}else 
						{
							$feedback = array('error' => "Registration failed",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
							$this->session->set_flashdata('msg',$feedback);
		                   redirect(base_url(('MC/newGuests')));
						}
            			
           }

}
//list of permitted entries per day
public function permEntries()
{
	$list['guests']=$this->mm->permEntries();
	$this->load->view('officer/perm_entries',$list);
}
//load new permit page
public function addPermEntry()
{
	$guestId=$this->input->post('guestId', TRUE);
	$list['guests']=$this->mm->getGuest($guestId);
	$this->load->view('officer/new_perm_entry',$list);
}
//save new permit
public function newPermit()
{
	$guestId=$this->input->post('guestId', TRUE);
	$visitType=$this->input->post('visitType', TRUE);
	$guestType=$this->input->post('guestType', TRUE);
	$personToVisit=$this->input->post('personToVisit', TRUE);
	$officeToVisit=$this->input->post('officeToVisit', TRUE);
	$timeOut=$this->input->post('timeOut', TRUE);
	$cardNo=$this->input->post('cardNo', TRUE);
	$minors=$this->input->post('minorsNo', TRUE);
	
	$entryDate= date("Y-m-d");

	$currentTime=date("h:i:s");

	$permit_details=array('entry_guest_auto_id'=>$guestId,'entry_date'=>$entryDate,'entry_time'=>$currentTime,'entry_expected_time_out'=>$timeOut, 'entry_office_auto_id'=>$officeToVisit,'entry_card_no'=>$cardNo,'entry_guest_type'=>$guestType,'entry_stratizen_to_visit'=>$personToVisit,'entry_visit_type'=>$visitType,'no_of_minors'=>$minors);

	$this->db->select('*');
	$this->db->from('entry_permits');
	$this->db->where('entry_guest_auto_id',$guestId);
	$this->db->where('entry_date',$entryDate);
	$this->db->where('entry_guest_release',0);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Guest has active access",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/guestsView')));
        }else 
            {
            	$result=$this->mm->newPermit($permit_details);
				if($result)
					{
						$feedback = array('error' => "",'success' => "Permit added",'state'=>array('fullName'=>"",'recordId'=>""));
						$this->session->set_flashdata('msg',$feedback);
	                   redirect(base_url(('MC/permEntries')));
					}else 
						{
							$feedback = array('error' => "Failed to add permit",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
							$this->session->set_flashdata('msg',$feedback);
		                   redirect(base_url(('MC/guestsView')));
						}
            			
           }


}
//error when permit for already exists
public function permError()
{
	$guestId=$this->input->post('guestId', TRUE);
	$entryRecordId=$this->input->post('permitRecordId', TRUE);
	$result=$this->mm->getGuest($guestId);
	
	$fullName;//to hold full name of guest
	foreach($result as $result)
	{
		$fullName=$result['guest_fname']." ".$result['guest_lname'];
	}
	$feedback = array('error' => "",'success' => "",'state'=>array('fullName'=>$fullName,'recordId'=>$entryRecordId));
	$this->session->set_flashdata('msg',$feedback);
	redirect(base_url(('MC/guestsView')));

	// if($result!="")
	// 	{
	// 		$fullName=$result->guest_fname;
	// 		$feedback = array('error' => "",'success' => "",'state'=>array('guestId'=>$guestId,'entry_auto_id '=>$entryRecordId,'fullName'=>$fullName));
	// 		$this->session->set_flashdata('msg',$feedback);
	// 	   redirect(base_url(('MC/newGuests')));
	// 	}else 
	// 		{
	// 			$feedback = array('error' =>"",'success' => "",'state'=>array('guestId'=>"",'entry_auto_id '=>"",'fullName'=>""));
	// 			$this->session->set_flashdata('msg',$feedback);
 //               redirect(base_url(('MC/newGuests')));
	// 		}


	
}
//release guest
public function guestExit()
{
	$entryRecordId=$this->input->post('entryRecordId', TRUE);
	$exitDetails=array('entry_guest_release'=>1);
	$result=$this->mm->guestRelease($exitDetails,$entryRecordId);
		if($result)
			{
				$feedback = array('error' => "",'success' => "Guest Released",'state'=>array('fullName'=>"",'recordId'=>""));
				$this->session->set_flashdata('msg',$feedback);
	           redirect(base_url(('MC/guestsView')));
			}else 
				{
					$feedback = array('error' =>"Could not release guest",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
					$this->session->set_flashdata('msg',$feedback);
	               redirect(base_url(('MC/guestsView')));
				}
}
public function getStratizen()
{
		$keyword=$this->input->get("q");
		$json = [];
		if(!empty($this->input->get("q"))){
			$this->db->or_like(array('stratizen_fname' => $keyword, 'stratizen_lname' => $keyword, 'stratizen_other_names' => $keyword));
			$query = $this->db->select('stratizen_auto_id as id,CONCAT(stratizen_su_id,": ",stratizen_fname," ",stratizen_lname) as text')
						->limit(10)
						->get("stratizens");
			$json = $query->result();
		}
		echo json_encode($json);
	
}
public function getOffice()
{
		$keyword=$this->input->get("q");
		$json = [];
		if(!empty($this->input->get("q"))){
			$this->db->or_like(array('office_name' => $keyword));
			$query = $this->db->select('office_auto_id as id,office_name as text')
						->limit(10)
						->get("offices");
			$json = $query->result();
		}
		echo json_encode($json);
	
}
}
