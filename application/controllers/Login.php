<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @name LoginCtrl.php
 * @Mokoro
 */

//controller to login users
class Login extends CI_Controller
{
    //constructor to initialize variables and load tools
    function __construct() 

    {
        parent::__construct();
        $this->load->model("LoginModel", "login");
        // $this->load->model("MainModel", "mainmodel");
    }
public function login() 
    {
    	//get submitted credentials
    	$username=$this->input->post('username',TRUE);
        $password=md5($this->input->post('password',TRUE));
        //try login 
        $admin = $this->login->validateAdmin($username, $password);//admin
        $officer = $this->login->validateOfficer($username, $password);//officer
			if($admin) 
                {
                    //take the returned data and create a session for it (adminName and adminID). 
                    foreach ($admin as $row)
                            { 
                                $fullName=$row->admin_fname."&nbsp;".$row->admin_lname;
                                $userID=$row->staff_id;
                                $sessdata=array();
                                $sessdata = array('adminName' =>$fullName,'adminID'=>$userID,'admin_login'=>TRUE); 
                                $this->session->set_userdata($sessdata);
                                        
                            }
                            redirect('MC/officersView');
                                
       		} else if($officer)
                    {
                        foreach ($officer as $row)
                                { 
                                    $fullName=$row->officer_fname."&nbsp;".$row->officer_lname;
                                    $userID=$row->officer_staff_id;
                                    $officerTeamName=$row->team_name;
                                    $sessdata=array();
                                    $sessdata = array('officerName' =>$fullName,'officerID'=>$userID,'officer_login'=>TRUE); 
                                    $this->session->set_userdata($sessdata);
                                }
                                redirect('MC/dashboard');
                    } else
                        {
                            
                            $feedback = array('error' => "Wrong credentials. Please try again");
                            $this->session->set_flashdata('msg',$feedback);
                            redirect(base_url('MC'));
    	                }
    }

}



