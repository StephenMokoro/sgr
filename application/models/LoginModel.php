<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LoginModel extends CI_Model
{
	function __construct()
		{
		     parent::__construct();
		     $this->load->database();
		}

    //login admin
	public function validateAdmin($username, $password) 
        {
            $this->db->select('*');
            $this->db->from('admin');
            $this->db->where('admin_username', $username);
            $this->db->where('password', $password);
            $this->db->where('active_status', 1);
            $this->db->limit(1);
            $query = $this->db->get();
             if ($query->num_rows() == 1) 
                {
                    return $query->result(); 
                } else 
                        {
                            return false;
                        }
        }
    //login officer
    public function validateOfficer($username, $password) 
        {
            $this->db->select('*');
            $this->db->from('officers');
            $this->db->where('officer_username', $username);
            $this->db->where('password', $password);
            $this->db->where('active_status', 1);
            $this->db->limit(1);
            $query = $this->db->get();
             if ($query->num_rows() == 1) 
                {
                    return $query->result(); 
                } else 
                        {
                            return false;
                        }
    }

}