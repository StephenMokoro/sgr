<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
*Main Model to handle db connections
* @author Mokoro Stephen
*/
class MainModel extends CI_Model
{
function __construct()
{
     parent::__construct();
     $this->load->database();
}
//admin registration
public function newAdmin($admin_details)
{
    if($this->db->insert('admin',$admin_details))
        {
            return true;
        }
         else
        {
            return false;
        }
}

//admin update
public function updateAdmin($admin_details,$admin_staff_id)
{
    $this->db->where('admin_staff_id',$admin_staff_id);
    $this->db->update('admin',$admin_details);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }
}

//admin profile
public function adminProfile($staffId)
{
    $this->db->select('*');
    $this->db->from('admin');
    $this->db->where('admin_staff_id',$staffId);
    $result=$this->db->get()->result_array();
    return $result;
}
//list of all active admins
public function activeAdminsList()
{
    $this->db->select('*');
    $this->db->from('admin');
    $this->db->where('active_status',1);
    $result=$this->db->get()->result_array();
    return $result;
}

//disable admin
public function disableAdmin($updateDetails,$staffId)
{
    $this->db->where('admin_staff_id',$staffId);
    $this->db->update('admin',$updateDetails);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;

            }else
                {
                    return false;
                }
}

//officer registration
public function newOfficer($officer_details)
{
	if($this->db->insert('officers',$officer_details))
        {
            return true;
        }
         else
        {
            return false;
        }
}

//officer update
public function updateOfficer($officer_details,$staffId)
{
    $this->db->where('officer_staff_id',$staffId);
    $this->db->update('officers',$officer_details);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }
}

//Security officer profile
public function officerProfile($staffId)
{
    $this->db->select('os.*, ps.phase_name');
    $this->db->from('officers os, phases ps');
    $this->db->where('os.active_status',1);
    $this->db->where('os.officer_phase_id=ps.phase_id');
    $this->db->where('os.officer_staff_id',$staffId);
    $result=$this->db->get()->result_array();
    return $result;
}
//list of all active officers
public function activeOfficersList()
{
    $this->db->select('os.*, ps.phase_name');
    $this->db->from('officers os, phases ps');
    $this->db->where('os.active_status',1);
    $this->db->where('os.officer_phase_id=ps.phase_id');
    $result=$this->db->get()->result_array();
    return $result;
}
//disable officer
public function disableOfficer($updateDetails,$staffId)
{
    $this->db->where('officer_staff_id',$staffId);
    $this->db->update('officers',$updateDetails);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;

            }else
                {
                    return false;
                }
}

//list of all guests
public function guestsList()
{
     $this->db->select('gs.*,id.id_type, ep.*');
    $this->db->from('guests gs, id_types id');
    $this->db->join('entry_permits ep', 'ep.entry_guest_auto_id = gs.guest_auto_id', 'left');
    $this->db->where('gs.guest_id_type=id.id_no');
    $this->db->order_by('gs.guest_auto_id','desc');
    $result=$this->db->get()->result_array();
    return $result;
}
//list of all guests for today
public function newGuestsList()
{
    $this->db->select('gs.*,id.id_type, ep.*');
    $this->db->from('guests gs, id_types id');
    $this->db->join('entry_permits ep', 'ep.entry_guest_auto_id = gs.guest_auto_id', 'left');
    $this->db->where('gs.guest_id_type=id.id_no');
    $this->db->where('gs.date_registered',date("Y-m-d"));
    $this->db->order_by('gs.guest_auto_id','desc');
    $result=$this->db->get()->result_array();
    return $result;
}

//buildings list
public function buildingsList()
{
    $this->db->select('*');
    $this->db->from('buildings');
    $result=$this->db->get()->result_array();
    return $result;
}
//helpdesk registration
public function newHelpdesk($helpdesk_details)
{
    if($this->db->insert('helpdesks',$helpdesk_details))
        {
            return true;
        }
         else
            {
                return false;
            }
}
//list of floors
public function floorsList()
{
    $this->db->select('*');
    $this->db->from('floors');
     $result=$this->db->get()->result_array();
    return $result;
}
//list of all helpdesks
public function helpdesksList()
{
    $this->db->select('hd.*, ph.phase_name,fl.floor_name,bd.building_name');
    $this->db->from('helpdesks hd, phases ph, floors fl,buildings bd');
    $this->db->where('ph.phase_id=hd.hd_phase_id');
    $this->db->where('hd.hd_floor_no=fl.floor_no');
    $this->db->where('hd.hd_building_id=bd.building_id');
    $this->db->order_by('hd.hd_name','asc');
    $result=$this->db->get()->result_array();
    return $result;
}
//get specific helpdesk for editing
public function getHelpdesk($hd_auto_id)
{
    $this->db->select('hd.*, ph.phase_name,fl.floor_name,bs.building_name');
    $this->db->from('helpdesks hd, phases ph, floors fl,buildings bs');
    $this->db->where('ph.phase_id=hd.hd_phase_id');
    $this->db->where('hd.hd_floor_no=fl.floor_no');
    $this->db->where('hd.hd_auto_id',$hd_auto_id);
    $this->db->where('hd.hd_building_id=bs.building_id');
    $result=$this->db->get()->result_array();
    return $result;
}
//function to update helpdesks
public function updateHelpdesk($helpdesk_details,$helpdeskId)
{
    $this->db->where('hd_auto_id',$helpdeskId);
    $this->db->update('helpdesks',$helpdesk_details);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }

}
//list of all active officers
public function activeHdOfficersList()
{
    $this->db->select('hos.*, hds.hd_name');
    $this->db->from('hd_officers hos, helpdesks hds');
    $this->db->where('hos.hd_active_status',1);
    $this->db->where('hos.hd_officer_hd_auto_id=hds.hd_auto_id');
    $this->db->order_by('hos.hd_officer_fname','asc');
    $result=$this->db->get()->result_array();
    return $result;
}

//officer Helpdesk registration
public function newHdOfficer($hd_officer_details)
{
    if($this->db->insert('hd_officers',$hd_officer_details))
        {
            return true;
        }
         else
        {
            return false;
        }
}

//officer Helpdesk update
public function updateHdOfficer($hd_officer_details,$staffId)
{
    $this->db->where('hd_officer_staff_id',$staffId);
    $this->db->update('hd_officers',$hd_officer_details);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }
}
//Helpdesk officer profile
public function hdOfficerProfile($staffId)
{
    $this->db->select('hos.*, hds.hd_name');
    $this->db->from('hd_officers hos, helpdesks hds');
    $this->db->where('hos.hd_active_status',1);
    $this->db->where('hos.hd_officer_hd_auto_id=hds.hd_auto_id');
    $this->db->where('hos.hd_officer_staff_id',$staffId);
    $result=$this->db->get()->result_array();
    return $result;
}
//disable Helpdesk officer
public function disableHdOfficer($updateDetails,$staffId)
{
    $this->db->where('hd_officer_staff_id',$staffId);
    $this->db->update('hd_officers',$updateDetails);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;

            }else
                {
                    return false;
                }
}
public function conferencesList()
{
    $this->db->select('*');
    $this->db->from('conferences');
    $this->db->where('conf_status', TRUE);
    $result=$this->db->get()->result_array();
    return $result;
}
public function getGuest($guestId)
{
    $this->db->select('*');
    $this->db->from('guests');
    $this->db->where('guest_auto_id',$guestId);
    $result=$this->db->get()->result_array();
    return $result;
}
//New Guest  Registration
public function newGuest($guest_details)
{
    if($this->db->insert('guests',$guest_details))
        {
            return true;
        }
         else
        {
            return false;
        }
}
public function permEntries()
{
    $this->db->select('ep.*,gs.*,is.id_type');
    $this->db->from('entry_permits ep,guests gs, id_types is');
    $this->db->where('gs.guest_auto_id=ep.entry_guest_auto_id');
    $this->db->where('gs.guest_id_type=is.id_no');
     $result=$this->db->get()->result_array();
    return $result;
}

public function newPermit($permit_details)
{
     if($this->db->insert('entry_permits',$permit_details))
        {
            return true;
        }
         else
        {
            return false;
        }
}
//Release guest for exit
public function guestRelease($exitDetails,$entryRecordId)
{
    $this->db->where('entry_auto_id',$entryRecordId);
    $this->db->update('entry_permits',$exitDetails);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }
}
}