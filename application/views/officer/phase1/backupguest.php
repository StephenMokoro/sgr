<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SGS - Guest Registration</title>
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('officer/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey">Guest Registration</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <span data-placement="top" data-toggle="tooltip" title="Add Guest">
                <button class="btn btn-primary btn-xs" data-title="Add Guest" data-toggle="modal" data-target="#guest-registration-modal" ><span class="fa fa-plus-circle"></span>&nbsp;Add Guest</button>
            </span>

             <span data-placement="top" data-toggle="tooltip" title="Refresh"><button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
            </span>
            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="<?php echo base_url('');?>"><span class="fa fa-print"></span>&nbsp;Print All</a>
            </span>
            <br><br>
            <div class="form-group col-md-12 col-lg-12">
                <?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}?>
                
            <div class="row">
                <div class="col-md-12">
                    <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="guesteslist"  >
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Full Name</th>
                                <th class="text-center">ID No.</th>
                                <th class="text-center">Phone Number</th>
                                <th class="text-center">Person to Visit</th>
                                <th class="text-center"></th>
                             </tr>
                        </thead>
                        <tbody >
                            <?php  $count=1;
                            foreach($guests as $guest){ 
                               ?>
                            <tr>
                                <td class="text-center"><?php echo $count;  ?></td>
                                <td class="text-left"><?php  echo $guest['guest_fname']. " ".$guest['guest_lname']; ?></td>
                                <td class="text-left"><?php  echo $guest['guest_id'];  ?></td>
                                <td class="text-left"><?php  echo $guest['guest_phone']; ?></td>
                                <td class="text-center">
                                    <form style="display:inline;" name=<?php echo '"formView_'. $guest['guest_id'].'"';  ?> method="post" action="<?php echo base_url('MC/guestProfile');?>">
                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="guestId" class="control-label">Record ID*</label>
                                                <input required="required" class="form-control" name="guestId" id="guestId" placeholder="101" value="<?php echo $guest['guest_id']; ?>">
                                            </div>
                                            <input class="btn btn-info btn-xs" data-title="View" id=<?php echo '"view_'. $guest['guest_id'].'"';  ?> name=<?php echo '"view_'. $guest['guest_id'].'"';  ?>  type="submit" value="View">
                                    </form>
                                     <form style="display:inline;" name=<?php echo '"formEdit_'. $guest['guest_id'].'"';  ?> method="post" action="<?php echo base_url('MC/editguest');?>">
                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="guestId" class="control-label">Record ID*</label>
                                                <input required="required" class="form-control" name="guestId" id="guestId" placeholder="101" value="<?php echo $guest['guest_id']; ?>">
                                            </div>
                                            <input class="btn btn-primary btn-xs" data-title="Edit" id=<?php echo '"edit_'. $guest['guest_id'].'"';  ?> name=<?php echo '"edit_'. $guest['guest_id'].'"';  ?>  type="submit" value="Edit">
                                    </form>  
                                    <button class="btn btn-warning btn-xs" data-title="Disable"  id=<?php echo '"disable_'. $guest['guest_id'].'"';  ?> name=<?php echo '"disable_'. $guest['guest_id'].'"';?> value=<?php echo '"'.$guest['guest_id'].'"';  ?> onclick="disOffId(this);">Disable
                                    </button>
                                    </button>
                                </td>
                            </tr>
                            <?php $count=$count+1;} ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
            </div>
            <div id="guest-registration-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-full">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: none!important;margin-bottom: -20px">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="full-width-modalLabel" ><strong style="color: darkred">Guest registration</strong></h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" id="guest_registration" method="post" action="<?php echo base_url(); ?>MC/new_guest">
                                <div class="row setup-content" >
                                    <div class="col-xs-12">
                                        <div class="col-md-12">
                                             <div class="form-group col-md-6 col-lg-6 ">
                                                <label for="firstName" class="control-label">First Name*</label>
                                                <input type="text" name="firstName" placeholder="e.g. Faith" class="form-control" id="firstName" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6" >
                                                <label for="lastName" class="control-label">Last Name*</label>
                                                <input type="text" name="lastName" placeholder="e.g. Kisu" class="form-control" id="lastName" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="otherNames" class="control-label">Other Names</label>
                                                <input type="text" name="otherNames" placeholder="e.g. Zawadi" class="form-control" id="otherNames" >
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="idNo" class="control-label">ID No.*</label>
                                                <input type="text" name="idNo" placeholder=" e.g. 32569874" class=" form-control" id="idNo" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6" >
                                                <label for="idType" class="control-label">ID Type*</label>
                                                <select type="text" name="idType" class="form-control" id="idType" required="required">
                                                    <option value="">--Select ID Type--</option>
                                                    <option value="Passport">Passport</option>
                                                    <option value="National">National</option>
                                                    <option value="Military">Military</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="phoneNumber" class="control-label"> Current Phone No.*</label>
                                                <input type="text" name="phoneNumber" placeholder="Current Phone Number" class=" form-control" id="phoneNumber" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6" >
                                                <label for="phaseNo" class="control-label">Phase Visiting*</label>
                                                <select type="text" name="phaseNo" class="form-control" id="phaseNo" required="required">
                                                    <option value="">--Select Phase--</option>
                                                    <option value="1">Phase I</option>
                                                    <option value="2">Phase II</option>
                                                    <option value="3">Phase III</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6" >
                                                <label for="phaseNo" class="control-label">Office Visiting*</label>
                                                <select type="text" name="phaseNo" class="form-control" id="phaseNo" required="required">
                                                    <option value="">--Select Phase--</option>
                                                    <option value="1">Phase I</option>
                                                    <option value="2">Phase II</option>
                                                    <option value="3">Phase III</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                            <div class="modal-header"></div>
                                                <br>
                                                <input type="submit" class="btn btn-primary" value="Submit">
                                                <input type="reset" class="btn btn-default" value="Reset">
                                                <input type="button" class="btn btn-danger pull-right" data-dismiss="modal" value="Close">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" id="disable_guest" tabindex="-1" role="dialog" aria-labelledby="Disable" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body ">
                            <button class="btn btn-danger" style="width: 100%!important;"><span class="pull-left"><span class="fa fa-warning"></span> Are you sure you want to disable this guest?</span></button>
                            <form role="form" method="post"  id="guest_disable" action="<?php echo base_url(); ?>MC/disableguest">
                                <div class="form-group col-md-12 col-lg-12" hidden="true">
                                    <label for="guestId" class="control-label">guest Id</label>
                                    <input type="text" name="guestId" placeholder="Staff Id" class=" form-control" id="guestId" required="required" readonly="true" >
                                </div>
                                <br><br>
                                <div class="form-group col-md-12 col-lg-12 " >
                                    <label for="reason_disable" class="control-label">Reason to disable guest*</label>
                                    <textarea required="required" class="form-control" name="reasonToDisable" placeholder="e.g. Transfer" ></textarea>
                                </div>
                                <button type="submit" class="btn btn-warning" id="disable_confirm"><span class="fa fa-check"></span> PROCEED</button>
                                <button type="button" class="btn " data-dismiss="modal"><span class="fa fa-remove"></span> Exit</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
