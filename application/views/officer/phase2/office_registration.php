<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SGS - Office Registration</title>
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('officer/officernav.php');?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey">Office Registration</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <span data-placement="top" data-toggle="tooltip" title="Add Office">
                    <button class="btn btn-primary btn-xs" data-title="Add Office" data-toggle="modal" data-target="#office-registration-modal" ><span class="fa fa-plus-circle"></span>&nbsp;Add Office</button></span>

             <span data-placement="top" data-toggle="tooltip" title="Refresh"><button class="btn btn-xs" data-title="Refresh"  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
            </span>
            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="<?php echo base_url('');?>"><span class="fa fa-print"></span>&nbsp;Print All</a>
            </span>
            <br><br>
            <div class="form-group col-md-12 col-lg-12">
                <?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}?>
            <div class="row">
                <div class="col-md-12">
                    <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="officeslist"  >
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Office Name</th>
                                <th class="text-center">Phase</th>
                                <th class="text-center">Floor</th>
                                <th class="text-center"></th>
                             </tr>
                        </thead>
                        <tbody >
                            <?php  $count=1;
                            foreach($offices as $office){ 
                               ?>
                            <tr>
                                <td class="text-center"><?php echo $count;  ?></td>
                                <td class="text-left"><?php  echo $office['office_name']; ?></td>
                                <td class="text-left"><?php  echo $office['phase'];  ?></td>
                                <td class="text-left"><?php  echo $office['floor']; ?></td>
                                <td class="text-center">
                                    <form style="display:inline;" name=<?php echo '"formView_'. $office['office_auto_id'].'"';  ?> method="post" action="<?php echo base_url('MC/officeMore');?>">
                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="officeId" class="control-label">Record ID*</label>
                                                <input required="required" class="form-control" name="officeId" id="officeId" placeholder="101" value="<?php echo $office['office_auto_id']; ?>">
                                            </div>
                                            <input class="btn btn-info btn-xs" data-title="View" id=<?php echo '"view_'. $office['office_auto_id'].'"';  ?> name=<?php echo '"view_'. $office['office_auto_id'].'"';  ?>  type="submit" value="View">
                                    </form>
                                     <form style="display:inline;" name=<?php echo '"formEdit_'. $office['office_auto_id'].'"';  ?> method="post" action="<?php echo base_url('MC/editoffice');?>">
                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="officeId" class="control-label">Record ID*</label>
                                                <input required="required" class="form-control" name="officeId" id="officeId" placeholder="101" value="<?php echo $office['office_auto_id']; ?>">
                                            </div>
                                            <input class="btn btn-primary btn-xs" data-title="Edit" id=<?php echo '"edit_'. $office['office_auto_id'].'"';  ?> name=<?php echo '"edit_'. $office['office_auto_id'].'"';  ?>  type="submit" value="Edit">
                                    </form>  
                                    <button class="btn btn-warning btn-xs" data-title="Disable"  id=<?php echo '"disable_'. $office['office_auto_id'].'"';  ?> name=<?php echo '"disable_'. $office['office_auto_id'].'"';?> value=<?php echo '"'.$office['office_auto_id'].'"';  ?> onclick="disAdmId(this);">Disable
                                    </button>
                                </td>
                            </tr>
                            <?php $count=$count+1;} ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
            </div>
            <div id="office-registration-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-full">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: none!important;margin-bottom: -20px">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="full-width-modalLabel" ><strong style="color: darkred">Office registration</strong></h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" id="office_registration" method="post" action="<?php echo base_url(); ?>MC/newOffice">
                                <div class="row setup-content" >
                                    <div class="col-xs-12">
                                        <div class="col-md-12">
                                             <div class="form-group col-md-6 col-lg-6 ">
                                                <label for="officeName" class="control-label">Office Name*</label>
                                                <input type="text" name="officeName" placeholder="e.g. iLab Africa" class="form-control" id="officeName" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6" >
                                                <label for="phaseNo" class="control-label">Phase*</label>
                                                <select type="text" name="phaseNo" class="form-control" id="phaseNo" required="required">
                                                    <option value="">--Select Phase--</option>
                                                    <option value="1">Phase I</option>
                                                    <option value="2">Phase II</option>
                                                    <option value="3">Phase III</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="floorNo" class="control-label">Floor No.</label>
                                                <input type="text" name="floorNo" placeholder="Floor No" class="form-control" id="floorNo" >
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                            <div class="modal-header"></div>
                                                <br>
                                                <input type="submit" class="btn btn-primary" value="Submit">
                                                <input type="reset" class="btn btn-default" value="Reset">
                                                <input type="button" class="btn btn-danger pull-right" data-dismiss="modal" value="Close">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
