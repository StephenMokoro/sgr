     <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom:0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right" >
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                     <?php echo $this->session->userdata('adminName');//session to show who is logged in?>
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="<?php //echo base_url('Home/viewuserprofile?userid='); echo $this->session->userdata('adminID'); ?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li> -->
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url('LoginCtrl/logoutadmin'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                      
                        <!--manage users-->
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw"></i> Manage Users<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url();?>MC/adminsView"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Admin</a>
                                </li>

                                 <li>
                                    <a href="<?php echo base_url();?>MC/officersView"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Officers</a>
                                </li>
                            </ul>
                            <!--second-level-->
                        </li>
                        <!--manage users-->
                        
                        <!--manage training-->
                        <li>
                            <a href="#"><i class="fa fa-tasks fa-fw"></i> Training Views <span class="fa arrow"></span></a>
                            <!-- /.nav-second-level -->
                           <!--  <ul class="nav nav-second-level">
                                <li>
                                     <a  href="<?php echo base_url(); ?>MC/trdays_view"><i class="fa fa-caret-right fa-fw" aria-hidden="true" ></i> Training Days</a>   
                                </li>
                                <li>
                                    <a href="<?php echo base_url('MC/tattendance_view');?>"><i class="fa fa-caret-right fa-fw" ></i> Training Attendance</a>
                                </li>
                            </ul> -->
                         <li class="text-center">
                            <a href="#" style="background-color: maroon;font-weight: bold;color:white"><span><i class="fa fa-copyright fa-fw"></i>2017 Phenom Research Lab</span></a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
