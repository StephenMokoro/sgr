
    <!-- bootstrap -->
    <link href="<?php echo base_url();?>assets/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap -->
    <link href="<?php echo base_url();?>assets/bootstrap/3.1.0/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo base_url();?>assets/datatables/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_url();?>assets/datatables/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/sb-admin-2/css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url();?>assets/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"> 
    
    <link href="<?php echo base_url();?>assets/datepicker/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"> 
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/su_icon" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/general-css/customcss.css">
    
