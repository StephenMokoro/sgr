-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2017 at 05:25 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `grs`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_staff_id` int(10) NOT NULL,
  `admin_nid` varchar(30) NOT NULL,
  `admin_fname` varchar(30) NOT NULL,
  `admin_lname` varchar(30) NOT NULL,
  `admin_other_names` varchar(50) NOT NULL,
  `admin_username` varchar(30) NOT NULL,
  `admin_phone` varchar(30) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `date_registered` date NOT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '1',
  `reason_inactive` varchar(100) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_staff_id`, `admin_nid`, `admin_fname`, `admin_lname`, `admin_other_names`, `admin_username`, `admin_phone`, `admin_email`, `date_registered`, `active_status`, `reason_inactive`, `password`, `date_updated`) VALUES
(78581, '31537790', 'Stephen', 'Mokoro', 'Nyang\'wono', 'smokoro', '0719508386', 'stephenmokoro@gmail.com', '2017-09-07', 1, NULL, '29460b26b4369f893ab221251f0eb8ec', '2017-09-07 13:05:35');

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE `buildings` (
  `building_id` int(5) NOT NULL,
  `building_name` varchar(100) NOT NULL,
  `build_phase_id` int(5) NOT NULL,
  `floors` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buildings`
--

INSERT INTO `buildings` (`building_id`, `building_name`, `build_phase_id`, `floors`) VALUES
(1, 'Lecture Theatres', 1, 3),
(2, 'Student Centre', 2, 5),
(3, 'Sir Thomas More', 2, 6),
(4, 'Administration Block', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `conferences`
--

CREATE TABLE `conferences` (
  `conf_auto_id` int(10) NOT NULL,
  `conf_name` varchar(100) NOT NULL,
  `conf_description` varchar(200) NOT NULL,
  `conf_venue` varchar(100) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `conf_status` tinyint(1) NOT NULL DEFAULT '1',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conferences`
--

INSERT INTO `conferences` (`conf_auto_id`, `conf_name`, `conf_description`, `conf_venue`, `date_from`, `date_to`, `conf_status`, `last_update`) VALUES
(1, 'ICT', 'ICT ', '11', '2017-09-17', '2017-09-17', 1, '2017-09-17 05:32:34');

-- --------------------------------------------------------

--
-- Table structure for table `entry_permits`
--

CREATE TABLE `entry_permits` (
  `entry_auto_id` int(10) NOT NULL,
  `entry_guest_auto_id` int(10) NOT NULL,
  `entry_date` date NOT NULL,
  `entry_time` time NOT NULL,
  `entry_expected_time_out` time NOT NULL,
  `entry_actual_time_out` time DEFAULT NULL,
  `entry_office_auto_id` int(5) DEFAULT NULL,
  `entry_card_no` int(10) NOT NULL,
  `entry_guest_type` int(2) NOT NULL,
  `entry_stratizen_to_visit` int(5) DEFAULT NULL,
  `entry_visit_type` int(2) NOT NULL,
  `no_of_minors` int(5) NOT NULL,
  `entry_guest_release` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entry_permits`
--

INSERT INTO `entry_permits` (`entry_auto_id`, `entry_guest_auto_id`, `entry_date`, `entry_time`, `entry_expected_time_out`, `entry_actual_time_out`, `entry_office_auto_id`, `entry_card_no`, `entry_guest_type`, `entry_stratizen_to_visit`, `entry_visit_type`, `no_of_minors`, `entry_guest_release`) VALUES
(1, 1, '2017-09-18', '00:00:00', '00:00:00', '00:00:00', NULL, 0, 2, 1, 1, 3, 1),
(2, 2, '2017-09-18', '00:00:00', '00:00:00', '00:00:00', 1, 677, 2, NULL, 1, 4, 1),
(3, 3, '2017-09-18', '06:43:49', '10:43:00', '00:00:00', NULL, 566, 2, 2, 1, 5, 1),
(4, 4, '2017-09-18', '01:29:09', '12:00:00', NULL, NULL, 432, 2, 1, 1, 2, 1),
(5, 5, '2017-09-18', '01:47:41', '04:00:00', NULL, 1, 478, 2, NULL, 1, 0, 1),
(6, 7, '2017-09-19', '09:21:30', '12:00:00', NULL, 2, 8545, 1, NULL, 1, 3, 1),
(7, 6, '2017-09-19', '09:24:36', '10:24:00', NULL, 1, 678, 2, NULL, 1, 0, 1),
(8, 8, '2017-09-19', '02:43:30', '05:42:00', NULL, NULL, 568, 1, 1, 1, 0, 1),
(9, 5, '2017-09-19', '02:44:51', '05:44:00', NULL, NULL, 343, 1, 1, 1, 0, 0),
(10, 8, '2017-09-19', '02:45:26', '05:45:00', NULL, NULL, 454, 1, 1, 2, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `floors`
--

CREATE TABLE `floors` (
  `floor_no` int(5) NOT NULL,
  `floor_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floors`
--

INSERT INTO `floors` (`floor_no`, `floor_name`) VALUES
(-1, 'Basement'),
(0, 'Ground Floor'),
(1, '1st Floor'),
(2, '2nd Floor'),
(3, '3rd Floor'),
(4, '4th Floor'),
(5, '5th Floor'),
(6, '6th Floor');

-- --------------------------------------------------------

--
-- Table structure for table `guests`
--

CREATE TABLE `guests` (
  `guest_auto_id` int(10) NOT NULL,
  `guest_id` varchar(30) NOT NULL,
  `guest_id_type` int(5) NOT NULL,
  `guest_fname` varchar(30) NOT NULL,
  `guest_lname` varchar(30) NOT NULL,
  `guest_other_names` varchar(50) NOT NULL,
  `guest_phone` varchar(30) NOT NULL,
  `id_upload` tinyint(1) DEFAULT '0',
  `date_registered` date NOT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '1',
  `reason_inactive` varchar(100) DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `guests`
--

INSERT INTO `guests` (`guest_auto_id`, `guest_id`, `guest_id_type`, `guest_fname`, `guest_lname`, `guest_other_names`, `guest_phone`, `id_upload`, `date_registered`, `active_status`, `reason_inactive`, `date_updated`) VALUES
(1, '35517144', 1, 'Ruby', 'Simiyu', 'Buyaki', '0722521122', 0, '2017-09-14', 1, NULL, '2017-09-16 15:01:09'),
(2, '31537790', 1, 'Stephen', 'Mokoro', 'Nyang\'wono', '719508386', 0, '2017-09-17', 1, NULL, '2017-09-17 16:39:17'),
(3, '3153779', 1, 'Stephen', 'Mokoro', 'Nyang\'wono', '719508380', 0, '2017-09-17', 1, NULL, '2017-09-17 18:32:54'),
(4, '31143543', 3, 'Desirae', 'Lang', 'Urielle Mckinney', '+973-46-6710711', 0, '2017-09-18', 1, NULL, '2017-09-18 10:23:25'),
(5, '2344', 4, 'Martha', 'Trevino', 'Philip Hatfield', '+268-31-5232518', 0, '2017-09-18', 1, NULL, '2017-09-18 10:31:22'),
(6, '112323', 3, 'Salvador', 'Black', 'Uriah Chen', '+191-76-4112290', 0, '2017-09-18', 1, NULL, '2017-09-18 13:38:31'),
(7, '2324', 1, 'Riley', 'Mcclain', 'Cecilia Haley', '+512-77-2451950', 0, '2017-09-19', 1, NULL, '2017-09-19 07:20:54'),
(8, '23212', 3, 'Harper', 'Snow', 'Kyle Buck', '+971-15-7672646', 0, '2017-09-19', 1, NULL, '2017-09-19 08:20:00');

-- --------------------------------------------------------

--
-- Table structure for table `guest_types`
--

CREATE TABLE `guest_types` (
  `guest_type_id` int(5) NOT NULL,
  `guest_type_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest_types`
--

INSERT INTO `guest_types` (`guest_type_id`, `guest_type_name`) VALUES
(1, 'Contractor'),
(2, 'General Guest'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `hd_officers`
--

CREATE TABLE `hd_officers` (
  `hd_officer_hd_auto_id` int(10) NOT NULL,
  `hd_officer_staff_id` int(10) NOT NULL,
  `hd_officer_nid` varchar(30) NOT NULL,
  `hd_officer_fname` varchar(30) NOT NULL,
  `hd_officer_lname` varchar(30) NOT NULL,
  `hd_officer_other_names` varchar(50) NOT NULL,
  `hd_officer_username` varchar(30) NOT NULL,
  `hd_officer_phone` varchar(30) NOT NULL,
  `hd_officer_email` varchar(100) NOT NULL,
  `date_registered` date NOT NULL,
  `hd_active_status` tinyint(1) NOT NULL DEFAULT '1',
  `hd_reason_inactive` varchar(100) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `hd_officers`
--

INSERT INTO `hd_officers` (`hd_officer_hd_auto_id`, `hd_officer_staff_id`, `hd_officer_nid`, `hd_officer_fname`, `hd_officer_lname`, `hd_officer_other_names`, `hd_officer_username`, `hd_officer_phone`, `hd_officer_email`, `date_registered`, `hd_active_status`, `hd_reason_inactive`, `password`, `date_updated`) VALUES
(2, 232, '23242', 'Tods', 'Sharp', 'Burke Byrd', 'wyqisa', '+148-97-7992439', 'jatocelivi@gmail.com', '2017-09-11', 1, NULL, '0de7e68555fb3eeedc48791634fdf613', '2017-09-17 04:20:29'),
(2, 78581, '31537790', 'Blair', 'Miles', 'Tate Mckay', 'mokoros', '+136-41-2322738', 'manysodase@yahoo.com', '2017-09-07', 1, NULL, '41f16e7a4d00681566bdaeee68d304ea', '2017-09-17 04:20:34'),
(1, 78582, '31313141', 'Dacey', 'Robles', 'Paki Dotson', 'smk', '+329-42-1849860', 'fezetywi@gmail.com', '2017-09-09', 1, NULL, '77d14e52f4400e211eb7307c3c9dfe8a', '2017-09-17 04:20:37');

-- --------------------------------------------------------

--
-- Table structure for table `helpdesks`
--

CREATE TABLE `helpdesks` (
  `hd_auto_id` int(5) NOT NULL,
  `hd_name` varchar(150) NOT NULL,
  `hd_phase_id` int(5) NOT NULL,
  `hd_building_id` int(5) DEFAULT NULL,
  `hd_floor_no` int(5) DEFAULT NULL,
  `date_registered` date NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `helpdesks`
--

INSERT INTO `helpdesks` (`hd_auto_id`, `hd_name`, `hd_phase_id`, `hd_building_id`, `hd_floor_no`, `date_registered`, `date_updated`) VALUES
(1, 'FIT Helpdesk', 1, 4, 1, '2017-09-13', '2017-09-17 04:19:26'),
(2, 'iBiz Africa', 2, 2, 5, '2017-09-14', '2017-09-14 08:55:15'),
(3, 'BCOM Helpdesk', 1, 4, 0, '2017-09-17', '2017-09-17 15:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `id_types`
--

CREATE TABLE `id_types` (
  `id_no` int(5) NOT NULL,
  `id_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `id_types`
--

INSERT INTO `id_types` (`id_no`, `id_type`) VALUES
(1, 'National ID'),
(2, 'Passport'),
(3, 'Military'),
(4, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `officers`
--

CREATE TABLE `officers` (
  `officer_phase_id` int(5) NOT NULL,
  `officer_staff_id` int(10) NOT NULL,
  `officer_nid` varchar(30) NOT NULL,
  `officer_fname` varchar(30) NOT NULL,
  `officer_lname` varchar(30) NOT NULL,
  `officer_other_names` varchar(50) NOT NULL,
  `officer_username` varchar(30) NOT NULL,
  `officer_phone` varchar(30) NOT NULL,
  `officer_email` varchar(100) NOT NULL,
  `date_registered` date NOT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '1',
  `reason_inactive` varchar(100) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `officers`
--

INSERT INTO `officers` (`officer_phase_id`, `officer_staff_id`, `officer_nid`, `officer_fname`, `officer_lname`, `officer_other_names`, `officer_username`, `officer_phone`, `officer_email`, `date_registered`, `active_status`, `reason_inactive`, `password`, `date_updated`) VALUES
(1, 2315, '2323424', 'Alana', 'Grant', 'Bree Leonard', 'lowasa', '+676-38-5753007', 'degyberi@yahoo.com', '2017-09-11', 1, NULL, '2ef0b3651ee0dd8bf8359e6deb33413a', '2017-09-11 15:59:41'),
(2, 78581, '31537790', 'Blair', 'Miles', 'Tate Mckay', 'mokoros', '+136-41-2322738', 'manysodase@yahoo.com', '2017-09-07', 0, 'Transfer\r\n', '41f16e7a4d00681566bdaeee68d304ea', '2017-09-09 08:45:51'),
(1, 78582, '31313141', 'Dacey', 'Robles', 'Paki Dotson', 'smk', '+329-42-1849860', 'fezetywi@gmail.com', '2017-09-09', 0, 'Transfer', '77d14e52f4400e211eb7307c3c9dfe8a', '2017-09-16 09:27:45');

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `office_auto_id` int(5) NOT NULL,
  `office_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`office_auto_id`, `office_name`) VALUES
(1, 'Admissions '),
(2, 'Cafeteria');

-- --------------------------------------------------------

--
-- Table structure for table `phases`
--

CREATE TABLE `phases` (
  `phase_id` int(5) NOT NULL,
  `phase_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phases`
--

INSERT INTO `phases` (`phase_id`, `phase_name`) VALUES
(1, 'Phase I'),
(2, 'Phase II'),
(3, 'Phase III');

-- --------------------------------------------------------

--
-- Table structure for table `stratizens`
--

CREATE TABLE `stratizens` (
  `stratizen_auto_id` int(10) NOT NULL,
  `stratizen_su_id` int(10) NOT NULL,
  `stratizen_type` int(5) NOT NULL,
  `stratizen_nid` varchar(30) NOT NULL,
  `stratizen_fname` varchar(30) NOT NULL,
  `stratizen_lname` varchar(30) NOT NULL,
  `stratizen_other_names` varchar(50) NOT NULL,
  `stratizen_phone` varchar(30) NOT NULL,
  `stratizen_email` varchar(100) NOT NULL,
  `date_registered` date NOT NULL,
  `stratizen_active_status` tinyint(1) NOT NULL DEFAULT '1',
  `stratizen_reason_inactive` varchar(100) DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `stratizens`
--

INSERT INTO `stratizens` (`stratizen_auto_id`, `stratizen_su_id`, `stratizen_type`, `stratizen_nid`, `stratizen_fname`, `stratizen_lname`, `stratizen_other_names`, `stratizen_phone`, `stratizen_email`, `date_registered`, `stratizen_active_status`, `stratizen_reason_inactive`, `date_updated`) VALUES
(1, 78581, 2, '31537790', 'Stephen', 'Mokoro', 'Nyang\'wono', '0719508386', 'stephenmokoro@gmail.com', '2017-09-18', 1, NULL, '2017-09-18 01:07:36'),
(2, 88581, 2, '32537790', 'Stephen', 'Mokoro', 'Nyang\'wono', '0719508386', 'stevemokoro@gmail.com', '2017-09-18', 1, NULL, '2017-09-18 03:24:29');

-- --------------------------------------------------------

--
-- Table structure for table `stratizen_types`
--

CREATE TABLE `stratizen_types` (
  `stratizen_type_id` int(5) NOT NULL,
  `stratizen_type_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stratizen_types`
--

INSERT INTO `stratizen_types` (`stratizen_type_id`, `stratizen_type_name`) VALUES
(1, 'Student'),
(2, 'Staff'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `visit_types`
--

CREATE TABLE `visit_types` (
  `visit_type_auto_id` int(11) NOT NULL,
  `visit_type_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visit_types`
--

INSERT INTO `visit_types` (`visit_type_auto_id`, `visit_type_name`) VALUES
(1, 'Official'),
(2, 'Personal'),
(3, 'Conference/Event'),
(4, 'Other');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_staff_id`),
  ADD UNIQUE KEY `admin_nid` (`admin_nid`),
  ADD UNIQUE KEY `admin_phone` (`admin_phone`),
  ADD UNIQUE KEY `admin_email` (`admin_email`),
  ADD UNIQUE KEY `admin_username` (`admin_username`);

--
-- Indexes for table `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`building_id`),
  ADD KEY `build_phase_id_fk` (`build_phase_id`);

--
-- Indexes for table `conferences`
--
ALTER TABLE `conferences`
  ADD PRIMARY KEY (`conf_auto_id`);

--
-- Indexes for table `entry_permits`
--
ALTER TABLE `entry_permits`
  ADD PRIMARY KEY (`entry_auto_id`),
  ADD KEY `entry_guest_auto_id_fk` (`entry_guest_auto_id`),
  ADD KEY `entry_guest_type_fk` (`entry_guest_type`),
  ADD KEY `entry_visit_type_fk` (`entry_visit_type`),
  ADD KEY `entry_office_auto_id_fk` (`entry_office_auto_id`),
  ADD KEY `entry_stratizen_to_visit_fk` (`entry_stratizen_to_visit`);

--
-- Indexes for table `floors`
--
ALTER TABLE `floors`
  ADD PRIMARY KEY (`floor_no`);

--
-- Indexes for table `guests`
--
ALTER TABLE `guests`
  ADD PRIMARY KEY (`guest_auto_id`),
  ADD UNIQUE KEY `guest_id` (`guest_id`),
  ADD UNIQUE KEY `guest_phone` (`guest_phone`),
  ADD KEY `guest_id_type_fk` (`guest_id_type`);

--
-- Indexes for table `guest_types`
--
ALTER TABLE `guest_types`
  ADD PRIMARY KEY (`guest_type_id`);

--
-- Indexes for table `hd_officers`
--
ALTER TABLE `hd_officers`
  ADD PRIMARY KEY (`hd_officer_staff_id`),
  ADD UNIQUE KEY `hd_officer_nid` (`hd_officer_nid`),
  ADD UNIQUE KEY `hd_officer_phone` (`hd_officer_phone`),
  ADD UNIQUE KEY `hd_officer_email` (`hd_officer_email`),
  ADD UNIQUE KEY `hd_officer_username` (`hd_officer_username`),
  ADD KEY `hd_officer_phase_id_fk` (`hd_officer_hd_auto_id`);

--
-- Indexes for table `helpdesks`
--
ALTER TABLE `helpdesks`
  ADD PRIMARY KEY (`hd_auto_id`),
  ADD KEY `hd_phase_id_fk` (`hd_phase_id`),
  ADD KEY `hd_floor_no_fk` (`hd_floor_no`),
  ADD KEY `hd_building_id_fk` (`hd_building_id`);

--
-- Indexes for table `id_types`
--
ALTER TABLE `id_types`
  ADD PRIMARY KEY (`id_no`);

--
-- Indexes for table `officers`
--
ALTER TABLE `officers`
  ADD PRIMARY KEY (`officer_staff_id`),
  ADD UNIQUE KEY `officer_nid` (`officer_nid`),
  ADD UNIQUE KEY `officer_phone` (`officer_phone`),
  ADD UNIQUE KEY `officer_email` (`officer_email`),
  ADD UNIQUE KEY `officer_username` (`officer_username`),
  ADD KEY `officer_phase_id_fk` (`officer_phase_id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`office_auto_id`);

--
-- Indexes for table `phases`
--
ALTER TABLE `phases`
  ADD PRIMARY KEY (`phase_id`),
  ADD UNIQUE KEY `phase_name` (`phase_name`);

--
-- Indexes for table `stratizens`
--
ALTER TABLE `stratizens`
  ADD PRIMARY KEY (`stratizen_auto_id`),
  ADD UNIQUE KEY `stratizen_su_id` (`stratizen_su_id`,`stratizen_nid`,`stratizen_phone`,`stratizen_email`),
  ADD KEY `stratizen_type_fk` (`stratizen_type`);

--
-- Indexes for table `stratizen_types`
--
ALTER TABLE `stratizen_types`
  ADD PRIMARY KEY (`stratizen_type_id`);

--
-- Indexes for table `visit_types`
--
ALTER TABLE `visit_types`
  ADD PRIMARY KEY (`visit_type_auto_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buildings`
--
ALTER TABLE `buildings`
  MODIFY `building_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `conferences`
--
ALTER TABLE `conferences`
  MODIFY `conf_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `entry_permits`
--
ALTER TABLE `entry_permits`
  MODIFY `entry_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `guests`
--
ALTER TABLE `guests`
  MODIFY `guest_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `helpdesks`
--
ALTER TABLE `helpdesks`
  MODIFY `hd_auto_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `office_auto_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `phases`
--
ALTER TABLE `phases`
  MODIFY `phase_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stratizens`
--
ALTER TABLE `stratizens`
  MODIFY `stratizen_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `visit_types`
--
ALTER TABLE `visit_types`
  MODIFY `visit_type_auto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `buildings`
--
ALTER TABLE `buildings`
  ADD CONSTRAINT `build_phase_id_fk` FOREIGN KEY (`build_phase_id`) REFERENCES `phases` (`phase_id`);

--
-- Constraints for table `entry_permits`
--
ALTER TABLE `entry_permits`
  ADD CONSTRAINT `entry_guest_auto_id_fk` FOREIGN KEY (`entry_guest_auto_id`) REFERENCES `guests` (`guest_auto_id`),
  ADD CONSTRAINT `entry_guest_type_fk` FOREIGN KEY (`entry_guest_type`) REFERENCES `guest_types` (`guest_type_id`),
  ADD CONSTRAINT `entry_office_auto_id_fk` FOREIGN KEY (`entry_office_auto_id`) REFERENCES `offices` (`office_auto_id`),
  ADD CONSTRAINT `entry_stratizen_to_visit_fk` FOREIGN KEY (`entry_stratizen_to_visit`) REFERENCES `stratizens` (`stratizen_auto_id`),
  ADD CONSTRAINT `entry_visit_type_fk` FOREIGN KEY (`entry_visit_type`) REFERENCES `visit_types` (`visit_type_auto_id`);

--
-- Constraints for table `guests`
--
ALTER TABLE `guests`
  ADD CONSTRAINT `guest_id_type_fk` FOREIGN KEY (`guest_id_type`) REFERENCES `id_types` (`id_no`);

--
-- Constraints for table `hd_officers`
--
ALTER TABLE `hd_officers`
  ADD CONSTRAINT `hd_officer_hd_auto_id_fk` FOREIGN KEY (`hd_officer_hd_auto_id`) REFERENCES `helpdesks` (`hd_auto_id`);

--
-- Constraints for table `helpdesks`
--
ALTER TABLE `helpdesks`
  ADD CONSTRAINT `hd_building_id_fk` FOREIGN KEY (`hd_building_id`) REFERENCES `buildings` (`building_id`),
  ADD CONSTRAINT `hd_floor_no_fk` FOREIGN KEY (`hd_floor_no`) REFERENCES `floors` (`floor_no`),
  ADD CONSTRAINT `hd_phase_id_fk` FOREIGN KEY (`hd_phase_id`) REFERENCES `phases` (`phase_id`);

--
-- Constraints for table `officers`
--
ALTER TABLE `officers`
  ADD CONSTRAINT `officer_phase_id_fk` FOREIGN KEY (`officer_phase_id`) REFERENCES `phases` (`phase_id`);

--
-- Constraints for table `stratizens`
--
ALTER TABLE `stratizens`
  ADD CONSTRAINT `stratizen_type_fk` FOREIGN KEY (`stratizen_type`) REFERENCES `stratizen_types` (`stratizen_type_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
